import json
import math
import os
import pygame
import RPi.GPIO as GPIO
import serial

"""

This is an example of using pygame to write to the
PiTFT screen buffer

"""

os.environ["SDL_FBDEV"] = "/dev/fb1"

GPIO.setmode(GPIO.BOARD)


class Display(object):

    PI = math.pi

    SMALL_FONT = 30
    MED_FONT = 50

    BLACK = (0, 0, 0)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)
    CYAN = (0, 255, 255)
    MAGENTA = (255, 0, 255)
    YELLOW = (255, 225, 0)
    WHITE = (255, 255, 255)

    def __init__(self, surface):
        super(Display, self).__init__()
        print("pygame init")
        pygame.init()

        # Set up the screen

        print("Set up the screen")
        self.surface = surface
        pygame.mouse.set_visible(0)

    def update_data(self):
        data = self.in_port.readline()
        if len(data) > 0:
            jsons = str(data.decode("utf-8")).replace("\r\n", "")
            try:
                return json.loads(jsons)
            except json.decoder.JSONDecodeError as _:
                return self.update_data()

    def printLoop(self):
        while True:
            in_port = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=0.5)
            output = " "
            print("----")
            while output != "":
                output = in_port.readline()
                print(output)
            in_port.close()

    def loop(self):
        print("main loop")
        while True:
            data = self.update_data()
            # print(data)

            square_that_is_the_size_of_the_screen = pygame.Surface(self.surface.get_size())
            square_that_is_the_size_of_the_screen.fill(Display.WHITE)
            self.surface.blit(square_that_is_the_size_of_the_screen, (0, 0))

            # # Draw speed arc
            # arcX = self.surface.get_width() / 4
            # arcY = self.surface.get_height() / 8
            # arcW = self.surface.get_width() / 2
            # arcH = self.surface.get_height() / 1.33
            # pygame.draw.arc(self.surface, Display.BLACK, [arcX, arcY, arcW, arcH], 5.75, 3.66, 1)

            # Draw speed needle

            # Draw speed text
            font = pygame.font.Font(None, Display.MED_FONT)
            speedStr = "42 mph"
            textSurface = font.render(speedStr, 1, Display.BLACK)

            textCenterX = self.surface.get_width() / 2
            textCenterY = (self.surface.get_height() / 2) + 45
            textCenter = (textCenterX, textCenterY)
            textpos = textSurface.get_rect(center=textCenter)

            self.surface.blit(textSurface, textpos)

            # # draw temp 1 arc
            # temp1ArcX = 2
            # temp1ArcY = self.surface.get_height() / 16
            # temp1ArcW = self.surface.get_width() / 4
            # temp1ArcH = self.surface.get_height() / 2.66
            # pygame.draw.arc(self.surface, Display.BLACK, [temp1ArcX, temp1ArcY, temp1ArcW, temp1ArcH], 5.75, 3.66, 1)

            # draw temp 1 text
            temp1Font = pygame.font.Font(None, Display.SMALL_FONT)
            temp1Str = "{} F".format(data['temp1'])
            temp1TextSurface = temp1Font.render(temp1Str, 1, Display.BLACK)

            temp1TextCenterX = self.surface.get_width() / 8
            temp1TextCenterY = (self.surface.get_height() / 4) + 20
            temp1TextCenter = (temp1TextCenterX, temp1TextCenterY)
            temp1Textpos = temp1TextSurface.get_rect(center=temp1TextCenter)

            self.surface.blit(temp1TextSurface, temp1Textpos)

            # # draw temp 2 arc
            # temp2ArcX = self.surface.get_width() - (self.surface.get_width() / 4)
            # temp2ArcY = self.surface.get_height() / 16
            # temp2ArcW = self.surface.get_width() / 4
            # temp2ArcH = self.surface.get_height() / 2.66
            # pygame.draw.arc(self.surface, Display.BLACK, [temp2ArcX, temp2ArcY, temp2ArcW, temp2ArcH], 5.75, 3.66, 1)

            # draw temp 2 text
            temp2Font = pygame.font.Font(None, Display.SMALL_FONT)
            temp2Str = "{} F".format(data['temp2'])
            temp2TextSurface = temp2Font.render(temp2Str, 1, Display.BLACK)

            temp2TextCenterX = self.surface.get_width() - (self.surface.get_width() / 8)
            temp2TextCenterY = (self.surface.get_height() / 4) + 20
            temp2TextCenter = (temp2TextCenterX, temp2TextCenterY)
            temp2Textpos = temp2TextSurface.get_rect(center=temp2TextCenter)

            self.surface.blit(temp2TextSurface, temp2Textpos)

            # Draw odometer
            # # Draw speed text
            # font = pygame.font.Font(None, Display.MED_FONT)
            # speedStr = "05000.0"
            # textSurface = font.render(speedStr, 1, Display.BLACK)

            # textCenterX = self.surface.get_width() / 2
            # textCenterY = (3 * self.surface.get_height() / 4) + 45
            # textCenter = (textCenterX, textCenterY)
            # textpos = textSurface.get_rect(center=textCenter)

            # self.surface.blit(textSurface, textpos)

            # Update the screen
            pygame.display.update()


if __name__ == '__main__':
    W = 480
    H = 320
    DISPLAYSURF = pygame.display.set_mode((W, H), 0, 16)
    display = Display(DISPLAYSURF)
    # display.loop()
    display.printLoop()
