import RPi.GPIO as GPIO
import serial
import time

"""
get data off the wire and display it on the screen.the

Use pygame to manage drawing.

"""

GPIO.setmode(GPIO.BOARD)
port = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=0.5)

while True:
    rcv = port.readline()
    print(rcv)
    time.sleep(1)
