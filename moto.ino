#include <Wire.h>
#include <Adafruit_MLX90614.h> // i2c multiplex
#include <Adafruit_MCP4725.h>  // dac
#include <SoftwareSerial.h> // talk to pi


#define TCAADDR 0x70
extern "C" { 
#include "utility/twi.h"  // from Wire library, so we can do bus scanning
}

const int ledPin =  13;         // the number of the onboard LED

//temperature
Adafruit_MLX90614 irTemp1 = Adafruit_MLX90614();
Adafruit_MLX90614 irTemp2 = Adafruit_MLX90614();
double temp1 = 0;
double temp2 = 0;
const int tcaTemp1 = 2;
const int tcaTemp2 = 7;
const int maxTemp = 100;

// tach
const int hallPin = 2;        // the number of the pushbutton pin
volatile int hallState = 0;     // variable for reading the hallsensor status
volatile unsigned int tickCount = 0;
volatile unsigned long oldTimeStamp = 0;
const int TACH_SAMPLE_SIZE = 5;
double hz = 0;

// display if
Adafruit_MCP4725 dac;
const int maxDacVal = 4095;
const int maxTempVal = 328; // observed value of max dac out
const int maxHzVal = 328;
SoftwareSerial piSerial(5, 6); // RX, TX

// display switch
const int buttonPin = 7;
volatile int buttonState = 0;
volatile int oldButtonState = 0;
volatile int currDispl = 0;
const int DISP_TEMP1 = 0;
const int DISP_TEMP2 = 1;
const int DISP_HZ = 2;


void tcaselect(uint8_t i) {
  if (i > 7) return;
 
  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();  
}

void setup() {
  pinMode(ledPin, OUTPUT);

  
  pinMode(buttonPin, INPUT);
  
  //*
  pinMode(hallPin, INPUT);
  // Attach an interrupt to the ISR vector
  attachInterrupt(digitalPinToInterrupt(hallPin), pin_ISR, FALLING);
  //*/
  

  Wire.begin();
  
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("\nTCAScanner ready!");

  tcaselect(tcaTemp1);
  irTemp1.begin();
  tcaselect(tcaTemp2);
  irTemp2.begin();

  dac.begin(0x62);
  
  piSerial.begin(9600);
  while (!piSerial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  piSerial.println("Hello?");

  oldTimeStamp = millis();
}

void voltDisplay(){

  float v = 0;
  double mytemp1 = temp1;
  double mytemp2 = temp2;

  // ((maxNew - minNew) / (maxOld - minOld)) * (in - maxOld) + maxNew 
  
  if (currDispl == DISP_TEMP1){
    if (mytemp1 > maxTempVal) mytemp1 = maxTempVal;
    if (mytemp1 < 0) mytemp1 = 0;
    v =  ((4095 - 0) / (maxTempVal - 0)) * (mytemp1 - maxTempVal) + 4095;
  } else if (currDispl == DISP_TEMP2){
    if (mytemp2 > maxTempVal) mytemp2 = maxTempVal;
    if (mytemp2 < 0) mytemp2 = 0;
    v =  ((4095 - 0) / (maxTempVal - 0)) * (mytemp2 - maxTempVal) + 4095;
  } else if (currDispl == DISP_HZ) {
    v = ((float) hz * 100 * maxDacVal) / maxHzVal;
//    Serial.print("\"hz\":\"");Serial.print(hz);Serial.print("\"");
  }

  piSerial.print("{");
  piSerial.print("\"temp1\":\"");piSerial.print(mytemp1);piSerial.print("\"");piSerial.print(",");
  piSerial.print("\"temp2\":\"");piSerial.print(mytemp2);piSerial.print("\"");piSerial.print(",");
  piSerial.print("\"hz\":\"");piSerial.print(hz);piSerial.print("\"");
  piSerial.print("}");
  piSerial.println("");

//*
  Serial.print("{");
  Serial.print("\"temp1\":\"");Serial.print(mytemp1);Serial.print("\"");Serial.print(",");
  Serial.print("\"temp2\":\"");Serial.print(mytemp2);Serial.print("\"");Serial.print(",");
  Serial.print("\"hz\":\"");Serial.print(hz);Serial.print("\"");
  Serial.print("}");
  Serial.println("");
//*/
  
  dac.setVoltage(v, false);
}

void loop() {

  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH && oldButtonState == LOW) {
    currDispl = (currDispl + 1) % 3;
    Serial.print("disp: ");Serial.println(currDispl);
    
    piSerial.print("pi disp: ");piSerial.println(currDispl);
  }

  oldButtonState = buttonState;

  // get temp data
  tcaselect(tcaTemp1);
  temp1 = irTemp1.readObjectTempF();
  tcaselect(tcaTemp2);
  temp2 = irTemp2.readObjectTempF();

  // calc rpm
  /*
   * calculate by getting the time diff between a sample
   * of fall events
   * 
   * then calculate freq from that.
   * 
   * ticks that occur during the mesurements aren't counted in any messurements.
   */

   if (tickCount >= TACH_SAMPLE_SIZE){
    detachInterrupt(digitalPinToInterrupt(hallPin));

    unsigned long now = millis();
    unsigned long timeBetweenFalls = now - oldTimeStamp;

    double xcpms =  (double) tickCount / (double) timeBetweenFalls; 
    hz = xcpms * 1000;
    double rpm = hz * 60;
    
    tickCount = 0;
    attachInterrupt(digitalPinToInterrupt(hallPin), pin_ISR, FALLING);
    oldTimeStamp = millis();
   }
   
//  serialDisplay();
  voltDisplay();
}

void pin_ISR() {
  ++tickCount;
}
